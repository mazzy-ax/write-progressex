mazzy@mazzy.ru, 2017-08-25, [https://github.com/mazzy-ax/Write-ProgressEx](https://github.com/mazzy-ax/Write-ProgressEx)

# Examples

## [Write-ProgressEx.pipe.ps1](/Write-ProgressEx.pipe.ps1)

![screenshot: Write-ProgressEx. pipe](../Media/examples.pipe.png)

## [Write-ProgressEx.parm.ps1](/Write-ProgressEx.parm.ps1)

![screenshot: Write-ProgressEx. parm](../Media/examples.parm.png)

## [Write-ProgressEx.info.ps1](/Write-ProgressEx.info.ps1)

![screenshot: Write-ProgressEx. Info](../Media/examples.pipe.png)

## [Write-ProgressEx.multi.ps1](/Write-ProgressEx.multi.ps1)

![screenshot: Write-ProgressEx. Multi](../Media/examples.multi-1.png)

![screenshot: Write-ProgressEx. Multi](../Media/examples.multi-2.png)
